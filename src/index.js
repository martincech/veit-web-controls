import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
// eslint-disable-next-line import/no-unresolved
import './styles.css';

ReactDOM.render(<App />, document.getElementById('root'));
