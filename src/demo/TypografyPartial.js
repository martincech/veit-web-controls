import React from 'react';

import {
  Label,
} from '../lib/index';

const TypografyPartial = () => {
  return (
    <React.Fragment>
      <div>
        <h1>TYPOGRAHY</h1>
      </div>
      <div>
        Font family: Montserrat
      </div>
      <br />
      <Label type="title">Title</Label>
      <Label type="info">SemiBold; font-size: 48px; color: #2E313D;</Label>
      <br />
      <Label type="subtitle">SUBTITLE</Label>
      <Label type="info">SemiBold; font-size: 1rem; color: #6A6E7B;</Label>
      <br />
      <Label type="text">Text</Label>
      <Label type="info">SemiBold; font-size: 0.875rem; color: #6A6E7B;</Label>

    </React.Fragment>
  );
};

export default TypografyPartial;
