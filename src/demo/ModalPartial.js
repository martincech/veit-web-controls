import React, { Component } from 'react';

import {

  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ButtonSwitch,
  Label,
  Input,
} from '../lib/index';

class ModalPartial extends Component {
  state = {
    modal: false,
  }

  toggle = () => {
    return this.setState(prevState => ({ modal: !prevState.modal }));
  };

  render() {
    return (
      <div>
        <h1>Modals</h1>
        <Button color="primary" onClick={this.toggle}>Open Modal window</Button>
        <Modal
          centered
          isOpen={this.state.modal}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Add Flock</ModalHeader>
          <ModalBody>
            <Label type="text">Measured by</Label>
            <ButtonSwitch options={[{ title: 'Whole house', value: 1 }, { title: 'Single sensor', value: 2 }]} selected={1}></ButtonSwitch>
            <Label type="info">Do you measure your flock only with one sensor or more</Label>
            <br />
            <Label type="text">Place to</Label>
            <ButtonSwitch options={[{ title: 'Existing house', value: 1 }, { title: 'New house', value: 2 }]} selected={2}></ButtonSwitch>
            <Label type="info">Do you want to use existing house or create a new one</Label>
            <br />
            <Label type="text">House name</Label>
            <Input options={[{ title: 'Existing house', value: 1 }, { title: 'New house', value: 2 }]} selected={2} />
            <Label type="info">Do you want to use existing house or create a new one</Label>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" outline onClick={this.toggle}>
              Back
            </Button>
            <div style={{ flex: '1' }}></div>
            <Button color="primary" onClick={this.toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default ModalPartial;
