import React from 'react';

import {
  Table,
  Label,
  Dot,
  Button,
  StatusIcon,
} from '../lib/index';

import { MdKeyboardArrowRight } from 'react-icons/lib/md';

const ColorsAndTablePartial = () => {
  const colors = [
    { color: 'blue', code: '#469ACF' },
    { color: 'green', code: '#46CF76' },
    { color: 'red', code: '#E04659' },
    { color: 'yellow', code: '#F1E521' },
    { color: 'black', code: '#2E313D' },
    { color: 'gray', code: '#6A6E7B' },
    { color: 'light-gray', code: '#9D9FAC' },
    { color: 'white-gray', code: '#E4E5EE' },
  ];

  const gradients = [
    'gradient-blue',
    'gradient-green-blue',
    'gradient-green',
  ];

  return (
    <React.Fragment>
      <div>
        <h1>COLORS</h1>
      </div>
      <div>
        <h3>Solid</h3>
      </div>
      <div>
        {colors.map(c => <Dot key={c.code} color={c.color} title={c.code}> </Dot>)}
      </div>
      <div>
        <h3>Gradient</h3>
      </div>
      <div>
        {gradients.map(g => <Dot key={g} color={g}> </Dot>)}
      </div>
      <br />
      <div>
        <h1>TABLE ROW</h1>
      </div>
      <div>
        <Table className="cr-table-info" style={{ height: '250px' }}>
          <thead>
            <tr>
              <th>Batch (type)</th>
              <th className="text-center">Update</th>
              <th className="text-center">Growth</th>
              <th>Progress</th>
              <th>Gain (g)</th>
              <th style={{ width: '1px', whiteSpace: 'nowrap' }}>See all flocks</th>
            </tr>
          </thead>
          <tbody>
            {
              [1, 2, 3].map(i => (
                <tr key={i}>
                  <td>
                    <Label>Loheman LSL</Label>
                    <Label type="info">Parrent Female #3</Label>
                  </td>
                  <td className="text-center"><StatusIcon hasError /></td>
                  <td className="text-center"><StatusIcon hasError={false} /></td>
                  <td>
                    <Label>30/10/2019</Label>
                    <Label type="info">Growth</Label>
                  </td>
                  <td><Label>+9</Label></td>
                  <td className="text-center">
                    <Button className="btn-dot" color="primary">
                      <MdKeyboardArrowRight style={{ verticalAlign: 'initial' }} />
                    </Button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </Table>
      </div>
    </React.Fragment>
  );
};

export default ColorsAndTablePartial;
