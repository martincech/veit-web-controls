import React from 'react';

import {
  ButtonSwitch,
  Input,
  Label,
  InputGroup,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from '../lib/index';

class InputPartial extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpenA: false,
      dropdownOpenB: false,
      sbtn1: 1,
      sbtn2: 1,
    };
  }

  onChange(name, value) {
    const state = { ...this.state };
    state[name] = value;
    this.setState(state);
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <h1>INPUTS</h1>
        </div>
        <br />
        <div>
          <h3>Basic input</h3>
        </div>
        <div>
          <Label>Input label</Label>
          <Input defaultValue="Input value" />
          <Input className="with-date" defaultValue="Date value" />
          <Input defaultValue="Disabled" disabled />
          <Label type="info" tag="span">Info/</Label>
          <Label type="error" tag="span">Error</Label>
        </div>
        <br />
        <div>
          <h3>Input + select</h3>
        </div>
        <div>
          <Label>Input label</Label>
          <InputGroup>
            <Input defaultValue="Input value" />
            <UncontrolledDropdown addonType="append">
              <DropdownToggle caret color="primary" outline className="form-control">
                Dropdown
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>Action</DropdownItem>
                <DropdownItem divider />
                <DropdownItem disabled>Disabled Action</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </InputGroup>
          <InputGroup>
            <Input disabled defaultValue="Input value" />
            <UncontrolledDropdown addonType="append">
              <DropdownToggle disabled caret color="primary" outline className="form-control">
                Dropdown
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>Another Action</DropdownItem>
                <DropdownItem divider />
                <DropdownItem>Another Action</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </InputGroup>
          <Label type="info" tag="span">Info/</Label>
          <Label type="error" tag="span">Error</Label>
        </div>
        <br />
        <div>
          <h3>Select</h3>
        </div>
        <div>
          <Label>Input label</Label>
          <UncontrolledDropdown>
            <DropdownToggle color="primary" outline caret>
              Dropdown
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Another Action</DropdownItem>
              <DropdownItem disabled>Disabled Action</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
          <Label type="info" tag="span">Info/</Label>
          <Label type="error" tag="span">Error</Label>
          <br />
          <br />
          <Label>Input disabled</Label>
          <UncontrolledDropdown>
            <DropdownToggle color="primary" outline caret disabled>
              Dropdown
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Another Action</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
          <Label type="info" tag="span">Info/</Label>
          <Label type="error" tag="span">Error</Label>
        </div>
        <br />
        <div>
          <h3>Switch</h3>
        </div>
        <div>
          <Label>3 options</Label>
          <ButtonSwitch
            options={[{ title: 'Item 1', value: 1 }, { title: 'Item 2', value: 2 }, { title: 'Item 3', value: 3 }]}
            onChange={(v) => { this.onChange('sbtn1', v); }}
            selected={this.state.sbtn1}
          />
          <br />
          <Label>2 options</Label>
          <ButtonSwitch
            options={[{ title: 'Item 1', value: 1 }, { title: 'Item 2', value: 2 }]}
            onChange={(v) => { this.onChange('sbtn2', v); }}
            selected={this.state.sbtn2}
          />
          <br />
          <Label>Disabled</Label>
          <ButtonSwitch
            disabled
            options={[{ title: 'Item 1', value: 1 }, { title: 'Item 2', value: 2 }]}
            onChange={(v) => { this.onChange('sbtn2', v); }}
            selected={this.state.sbtn2}
          />

          <Label type="info" tag="span">Info/</Label>
          <Label type="error" tag="span">Error</Label>
        </div>
      </React.Fragment>
    );
  }
}

export default InputPartial;
