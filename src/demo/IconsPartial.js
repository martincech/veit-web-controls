import * as icons from '../lib/icons/index';
import React from 'react';
import Label from '../lib/Components/Label';

const IconsPartial = () => {
  const ics = Object.keys(icons).map(k => icons[k]);

  return (
    <div>
      <div>
        <h1>ICONS</h1>
      </div>
      <div style={{ fontSize: '30px', display: 'flex', flexFlow: 'wrap' }}>
        {
          ics.map((Icon, i) => (
            <span
              key={i}
              style={{
                margin: '5px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Icon />
              <Label tag="span" type="info">
                {Icon.name}
              </Label>
            </span>
          ))
        }
      </div>
    </div>
  );
};

export default IconsPartial;
