import React, { Component } from 'react';
import {
  Button,
  TextButton,
  AddButton,
  Label,
  CalendarPopover,
  RangePopover,
} from '../lib/index';

class ButtonsPartial extends Component {
  state = {
    from: new Date(Date.now() - 1000 * 60 * 60 * 24 * 12),
    to: new Date(Date.now() + 1000 * 60 * 60 * 24 * 12),
    range: { min: 10, max: 50, maxLimit: 200 },
  }

  setDate = (date) => {
    if (Array.isArray(date)) {
      this.setState({ from: date[0], to: date[1] });
    } else {
      this.setState({ from: date });
    }
  };

  setRange = range => this.setState({ range });

  render() {
    return (
      <React.Fragment>
        <div>
          <h1>BUTTONS</h1>
        </div>
        <br />
        <div>
          <h3>Button + icon</h3>
        </div>
        <div>
          <AddButton>
            Add
          </AddButton>
          <AddButton disabled>
            Add Disabled
          </AddButton>
        </div>
        <br />
        <div>
          <h3>Button</h3>
        </div>
        <div>
          <Button color="primary">Button</Button>
          <Button color="primary" disabled>Button diabled</Button>
        </div>
        <br />
        <div>
          <h3>Button narrow</h3>
        </div>
        <div>
          <Button className="btn-narrow" color="primary">Button value</Button>
        </div>
        <br />
        <div>
          <h3>Ghost button</h3>
        </div>
        <div>
          <Button outline color="primary">Button</Button>
          <Button outline color="primary" disabled>Button disabled</Button>
        </div>
        <br />
        <div>
          <h3>Text button</h3>
        </div>
        <div>
          <TextButton>Button</TextButton>
          <br />
          <TextButton active>Active Button</TextButton>
        </div>
        <br />

        <CalendarPopover
          selectRange
          value={[this.state.from, this.state.to]}
          onChange={this.setDate}
        >
          <Button color="primary">Calendar Range</Button>
        </CalendarPopover>
        <CalendarPopover
          value={this.state.from}
          onChange={this.setDate}
        >
          <Button color="primary">Calendar</Button>
        </CalendarPopover>
        <Label>{`From : ${this.state.from.toISOString()}`}</Label>
        <Label>{`To : ${this.state.to.toISOString()}`}</Label>
        <br />
        <RangePopover range={this.state.range} onChange={this.setRange}>
          <Button color="primary">Range</Button>
        </RangePopover>
        <Label>{`Range Min: ${this.state.range.min}  Max: ${this.state.range.max}`}</Label>
        <br />
      </React.Fragment>
    );
  }
}

export default ButtonsPartial;
