import React, { Component } from 'react';

import {
  Inline,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Status,
  Avatar,
} from '../lib/index';

import {
  MdBugReport,
  MdEvent,
  MdSentimentSatisfied,
} from 'react-icons/lib/md';

class TopBarPartial extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  render() {
    return (
      <Inline style={{
        height: '80px',
        backgroundColor: 'white',
        marginLeft: '-15px',
        marginRight: '-15px',
      }}
      >
        <Status icon={MdBugReport} text="Status" hasError={false} />
        <Status icon={MdEvent} text="Events" hasError />
        <Status icon={MdSentimentSatisfied} text="Happy" hasError={false} />
        <div style={{ flex: '1' }}></div>
        <Avatar />
        <Dropdown isOpen={this.state.dropdownOpen} toggle={() => this.toggle()}>
          <DropdownToggle className="dropdown-toggle--clear" color="primary" outline caret>
            john@veit-genetix.com
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>Header</DropdownItem>
            <DropdownItem disabled>Action</DropdownItem>
            <DropdownItem>Another Action</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Another Action</DropdownItem>
          </DropdownMenu>
        </Dropdown>

      </Inline>
    );
  }
}

export default TopBarPartial;
