import React from 'react';
import {
  Table,
  Label,
  StatusIcon,
} from '../lib/index';


const TablesPartial = () => {
  const data = [
    {
      day: 1,
      growth: true,
      sex: 1,
      farm: { name: 'Farm Holzminden', location: '(Holzminden, DE)' },
    },
    {
      day: 2,
      growth: false,
      sex: 2,
      farm: { name: 'Farm Holzminden', location: '(Holzminden, DE)' },
    },
    {
      day: 3,
      growth: false,
      sex: 3,
      farm: { name: 'Farm Holzminden', location: '(Holzminden, DE)' },
    },
    {
      day: 4,
      growth: true,
      sex: 0,
      farm: { name: 'Farm Holzminden', location: '(Holzminden, DE)' },
    },
  ];

  const rnd = () => {
    return Math.floor(Math.random() * 100) + 10;
  };

  return (
    <div>
      <Table type="data" style={{ height: '200px' }}>
        <thead>
          <tr>
            <th>Day</th>
            <th className="text-center">Growth</th>
            <th>Gain(g)</th>
            <th>Weight(g)</th>
            <th>Count(pcs)</th>
            <th>Unifor.(%)</th>
            <th>Temp(°C)</th>
            <th>Humid(%)</th>
            <th>CO(%)</th>
            <th>Farm</th>
          </tr>
        </thead>
        <tbody>
          {data.map((d, i) => (
            <tr key={i}>
              <td><Label>{d.day}</Label></td>
              <td className="text-center"><StatusIcon hasError={d.growth} /></td>
              <td>
                <Label>
                  +
                  {rnd()}
                </Label>
              </td>
              <td><Label>{rnd()}</Label></td>
              <td><Label>{rnd()}</Label></td>
              <td><Label>{rnd()}</Label></td>
              <td><Label>{rnd()}</Label></td>
              <td><Label>{rnd()}</Label></td>
              <td><Label>{rnd()}</Label></td>
              <td>
                <Label>{d.farm.name}</Label>
                <Label type="info">{d.farm.location}</Label>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default TablesPartial;
