import React from 'react';
import bem from '../utils/bem';
import uniqueID from '../utils/uuid';
import IMApplyAccented from '../icons/IMApplyAccented';

const bm = bem.create('checkbox');
const CheckBox = ({ children, ...restProps }) => {
  const uuid = uniqueID();
  return (
    <label htmlFor={uuid} className={bm.b()}>
      {children}
      <input id={uuid} className={bm.e('input')} type="checkbox" {...restProps} />
      <span className={bm.e('checkmark')}>
        <IMApplyAccented />
      </span>
    </label>
  );
};

export default CheckBox;
