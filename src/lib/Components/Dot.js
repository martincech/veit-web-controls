import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Dot = ({
  color,
  className,
  children,
  title,
  ...restProps
}) => {
  const bgColor = `bg-${color}`;
  const classes = classNames('vwc-dot', bgColor, className);
  return (
    <span className={classes} {...restProps}>
      {children}
      <span>{title}</span>
    </span>
  );
};

Dot.propTypes = {
  color: PropTypes.string.isRequired,
};

export default Dot;
