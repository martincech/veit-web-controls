import React from 'react';

import { IMCheckMark, IMErrorMark } from '../icons';

import bem from '../utils/bem';

const StatusIcon = ({ hasError, className, ...otherProps }) => {
  const bm = bem.create('status-icon');
  const classes = bm.m(hasError ? 'error' : 'ok', bm.b(), className);
  const Icon = hasError ? IMErrorMark : IMCheckMark;
  return <Icon className={classes} {...otherProps} />;
};

export default StatusIcon;
