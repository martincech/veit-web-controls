import React from 'react';
import classNames from 'classnames';
import { Button } from '../index';
import { IMPlus } from '../icons';

const AddButton = ({ children, className, ...otherProps }) => {
  const classes = classNames('btn-icon', className);
  return (
    <Button className={classes} color="primary" {...otherProps}>
      <span className="btn-icon__icon"><IMPlus /></span>
      {children}
    </Button>
  );
};

export default AddButton;
