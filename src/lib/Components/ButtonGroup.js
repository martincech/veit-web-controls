import React from 'react';
import { ButtonGroup as Base } from 'reactstrap';
import classNames from 'classnames';

const ButtonGroup = ({ className, children, ...otherProps }) => {
  const classes = classNames('vwc-button-group', className);
  return (
    <Base className={classes} {...otherProps}>{children}</Base>
  );
};

export default ButtonGroup;
