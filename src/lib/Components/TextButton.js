import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TextButton = ({
  tag: Tag, active, className, ...restProps
}) => {
  const classes = classNames('vwc-text-button', className, { active });
  return (
    <Tag className={classes} {...restProps} />
  );
};

TextButton.propTypes = {
  active: PropTypes.bool,
  className: PropTypes.string,
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
};

TextButton.defaultProps = {
  active: false,
  className: null,
  tag: 'span',
};

export default TextButton;
