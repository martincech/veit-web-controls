import React from 'react';
import { bem } from '../index';

const Status = ({
  icon: Icon,
  text,
  hasError,
  className,
  ...otherProps
}) => {
  const bm = bem.create('status');
  const classes = bm.b(className, bm.m(hasError ? 'error' : 'ok'));
  return (
    <div className={classes} {...otherProps}>
      <Icon className={bm.e('icon')} />
      <div className={bm.e('text')}>{text}</div>
    </div>
  );
};

export default Status;
