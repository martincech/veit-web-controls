import React, { Component } from 'react';
import { Table as Base } from 'reactstrap';
import classNames from 'classnames';
import PropTypes from 'prop-types';

class Table extends Component {
  render() {
    const {
      className,
      children,
      type,
      style,
      ...otherProps
    } = this.props;
    const classes = classNames('vwc-table', `vwc-table-${type}`, className);
    const wrapper = classNames('vwc-table-wrap', type === 'data' ? 'shadow' : null);
    return (
      <div style={style} className={wrapper}>
        <Base className={classes} {...otherProps}>
          {children}
        </Base>
      </div>
    );
  }
}

Table.defaultProps = {
  type: 'info',
};

Table.propTypes = {
  type: PropTypes.oneOf(['info', 'data']),
};

export default Table;
