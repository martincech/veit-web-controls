import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Label = ({
  type,
  className,
  children,
  tag: Tag,
  ...restProps
}) => {
  const font = `label--${type}`;
  const classes = classNames('vwc-label', font, className);
  return (
    <Tag className={classes} {...restProps}>
      {children}
    </Tag>
  );
};

Label.propTypes = {
  tag: PropTypes.string,
  type: PropTypes.oneOf(['title', 'subtitle', 'text', 'info', 'error']),
};

Label.defaultProps = {
  type: 'text',
  tag: 'div',
};

export default Label;
