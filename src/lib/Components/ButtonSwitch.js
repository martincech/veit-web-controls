import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Button, ButtonGroup } from '../index';

class ButtonSwitch extends Component {
  clickHandled(value) {
    if (this.props.selected === value) return;
    if (this.props.onChange) this.props.onChange(value);
  }

  render() {
    const { disabled } = this.props;
    return (
      <ButtonGroup>
        {
          this.props.options
          && this.props.options.map(({ value, title }) => (
            <Button
              disabled={disabled}
              key={value}
              color="primary"
              onClick={() => { this.clickHandled(value); }}
              active={this.props.selected === value}
              outline={this.props.selected !== value}
            >
              {title}
            </Button>
          ))
        }
      </ButtonGroup>
    );
  }
}

ButtonSwitch.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.node,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
  })),
  selected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};


ButtonSwitch.defaultProps = {
  options: [],
  selected: null,
  onChange: null,
  disabled: false,
};

export default ButtonSwitch;
