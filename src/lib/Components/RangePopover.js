import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Popover, PopoverBody, PopoverHeader, CheckBox,
} from '../index';
import InputRange from 'react-input-range';
import uniqueID from '../utils/uuid';

class RangePopover extends Component {
  constructor(props) {
    super(props);
    const { range } = props;
    this.state = {
      isOpen: false,
      id: uniqueID(),
      range: {
        min: 1,
        max: 100,
        minLimit: 1,
        maxLimit: 100,
        ...range,
      },
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.range !== prevProps.range) {
      this.setState({ range: { ...this.props.range } });
    }
  }

  setRange = ({ min, max }) => {
    this.setState((prevState) => {
      const minValue = min < 1 ? 1 : min;
      const maxValue = max > prevState.range.maxValue
        ? prevState.range.maxValue
        : max;
      return {
        range: {
          ...prevState.range,
          min: minValue,
          max: maxValue,
          minLock: prevState.range.minLock != null ? minValue : null,
          maxLock: prevState.range.maxLock != null ? maxValue : null,
        },
      };
    });
  }

  setRangeComplete = () => {
    if (this.props.onChange) {
      this.props.onChange({ ...this.state.range });
    }
  }

  toggleLock = () => {
    this.setState((prevState) => {
      const lock = prevState.range.maxLock
        ? { minLock: null, maxLock: null }
        : { minLock: prevState.range.min, maxLock: prevState.range.max };
      const range = {
        ...prevState.range,
        ...lock,
      };
      this.props.onChange({ ...range });
      return { range };
    });
  }
  toggle = () => this.setState(prevState => ({ isOpen: !prevState.isOpen }));

  render() {
    const { range } = this.state;
    return (
      <React.Fragment>
        <span id={`range_${this.state.id}`} role="presentation" onClick={this.props.disabled ? null : this.toggle}>
          {this.props.children}
        </span>
        <Popover
          style={{ width: '200px' }}
          isOpen={this.state.isOpen}
          toggle={this.toggle}
          target={`range_${this.state.id}`}
          placement="top"
        >
          <PopoverHeader>
            {`Range : ${range.min} - ${range.max}. ${this.props.unit}`}
          </PopoverHeader>
          <PopoverBody>
            <InputRange
              minValue={range.minLimit}
              maxValue={range.maxLimit}
              value={range}
              onChange={this.setRange}
              onChangeComplete={() => this.setRangeComplete(this.state.range)}
            />
            <div className="input-range__lock">
              <CheckBox
                checked={range.maxLock != null}
                onClick={() => this.toggleLock()}
                readOnly
                style={{ height: 'unset', marginRight: '10px' }}
              >
                Remember range
              </CheckBox>
            </div>
          </PopoverBody>
        </Popover>
      </React.Fragment>
    );
  }
}

RangePopover.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
  onChange: PropTypes.func.isRequired,
  range: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    minValue: PropTypes.number,
    maxValue: PropTypes.number,
    minLock: PropTypes.number,
    maxLock: PropTypes.number,
  }).isRequired,
  placement: PropTypes.string,
  unit: PropTypes.string,
  disabled: PropTypes.bool,
};

RangePopover.defaultProps = {
  placement: 'top',
  unit: 'day',
  disabled: false,
};

export default RangePopover;
