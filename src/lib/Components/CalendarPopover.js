import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Calendar from 'react-calendar/dist/entry.nostyle';
import { Popover } from '../index';
import uniqueID from '../utils/uuid';


class CalendarPopover extends Component {
  state = {
    isOpen: false,
    id: uniqueID(),
  }

  toggle = () => this.setState(prevState => ({ isOpen: !prevState.isOpen }));

  render() {
    const {
      children, placement, disabled, ...restProps
    } = this.props;
    return (
      <React.Fragment>
        <span id={`calendar_${this.state.id}`} role="presentation" onClick={disabled ? null : this.toggle}>
          {children}
        </span>
        <Popover placement={placement} isOpen={this.state.isOpen} target={`calendar_${this.state.id}`} toggle={this.toggle}>
          <Calendar {...restProps} />
        </Popover>
      </React.Fragment>
    );
  }
}

CalendarPopover.propTypes = {
  placement: PropTypes.string,
  disabled: PropTypes.bool,
};

CalendarPopover.defaultProps = {
  placement: 'bottom',
  disabled: false,
};

export default CalendarPopover;
