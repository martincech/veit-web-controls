import React from 'react';
import classNames from 'classnames';

const Inline = ({ className, children, ...otherProps }) => {
  const classes = classNames('vwc-inline', className);
  return (
    <div className={classes} {...otherProps}>
      {children}
    </div>
  );
};

export default Inline;
