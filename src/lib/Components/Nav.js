import React from 'react';
import classNames from 'classnames';
import { Nav as Base } from 'reactstrap';

const Nav = ({ className, ...otherProps }) => {
  const classes = classNames('vwc-nav', className);
  return (
    <Base className={classes} {...otherProps} />
  );
};

export default Nav;
