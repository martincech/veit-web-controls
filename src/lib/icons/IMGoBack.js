//Autogenerated icon component
import React from 'react';
import Icon from 'react-icon-base';

const IMGoBack = props => (
	<Icon viewBox="0 0 1092 1024" {...props}>
		<g><path d="M1071.753 535.234h-928.361l406.351 406.351-46.47 46.47-485.867-485.867 485.867-485.867 46.47 46.47-406.351 406.351h928.361z"/></g>
	</Icon>
);

export default IMGoBack;