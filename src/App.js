import React from 'react';

import {
  Container,
  Row,
  Col,
} from './lib/index';

import InputPartial from './demo/InputPartial';
import ButtonsPartial from './demo/ButtonsPartial';
import ColorsAndTablePartial from './demo/ColorsAndTablePartial';
import TypografyPartial from './demo/TypografyPartial';
import TablesPartial from './demo/TablesPartial';
import ModalPartial from './demo/ModalPartial';
import TopBarPartial from './demo/TopBarPartial';
import IconsPartial from './demo/IconsPartial';

const App = () => {
  return (
    <main>
      <Container className="vwc-content" fluid style={{ backgroundColor: '#f5f6fa' }}>
        <TopBarPartial />
        <Row>
          <Col md="12">
            <Container fluid>
              <Row>
                <Col md="6">
                  <ColorsAndTablePartial />
                </Col>
                <Col md="6">
                  <TypografyPartial />
                  <br />
                  <ModalPartial />
                  <br />
                  <IconsPartial />
                </Col>
              </Row>
            </Container>
            <br />
            <TablesPartial />
            <br />
            <Container fluid>
              <Row>
                <Col md="6">
                  <InputPartial />
                </Col>
                <Col md="6">
                  <ButtonsPartial />
                </Col>
              </Row>
            </Container>
            <br />
          </Col>
        </Row>
      </Container>
    </main>
  );
};

export default App;
