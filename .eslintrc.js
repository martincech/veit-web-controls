module.exports = {
  root: true,
  extends: [
    'airbnb'
  ],
  parser: 'babel-eslint',
  plugins: [],
  rules: {
    'arrow-body-style': 0,
    'no-shadow': 0,
    'no-nested-ternary': 0,
    'lines-between-class-members': 0,
    'linebreak-style': 0,
    'no-alert': 0,
    'import/first': 0,
    'import/order': 0,
    'import/no-cycle': 0,
    'react/destructuring-assignment': 0,
    'react/prefer-stateless-function': 0,
    'react/no-unused-prop-types': 0,
    'react/no-did-update-set-state': 0,
    'react/no-multi-comp': 0,
    'react/jsx-filename-extension': 0,
    'react/self-closing-comp': 0,
  },
  globals:{
    "document": true
  }
};
