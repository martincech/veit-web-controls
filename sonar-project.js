const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner({
  serverUrl: 'https://sonarcloud.io',
  options: {
    'sonar.organization': 'veit',
    'sonar.projectKey': 'veit-web-controls',
    'sonar.login': process.env.SONAR_TOKEN,
    'sonar.sources': 'src',
  },
}, () => { });
